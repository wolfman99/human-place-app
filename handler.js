'use strict';

module.exports.hello = async (event, context) => {
  try {
    let cuerpo = JSON.parse(event.body);
    let intencion = cuerpo["queryResult"]["intent"]["displayName"]
    let idIntencion = cuerpo["queryResult"]["intent"]["name"]
    let parametros = cuerpo["queryResult"]["parameters"]
    console.log(cuerpo)
    console.log("Intención", intencion)
    console.log("Parámetros", parametros)
  } catch (error){
    console.log("Petición sin cuerpo")
  }
  
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless v1.0! Your function executed successfully!',
        "fulfillmentText": "response text",
        "fulfillmentMessages": [{"simpleResponses": {"simpleResponses": [   {
           "textToSpeech": "response text",
           "displayText": "response text"
        }]}}]
    }),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
