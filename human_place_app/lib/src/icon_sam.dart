import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

// ignore: must_be_immutable
class IconsSam extends StatelessWidget {

  IconsSam.sam({this.height: 50, this.width: 50}) {
    urlIcon = 'assets/icons/icon_sam.png';
  }
  IconsSam.samLoading({this.height: 30, this.width: 30}) {
    urlIcon = 'assets/icons/icon_sam_loading.png';
  }
  IconsSam.samQuestion({this.height: 50, this.width: 50}) {
    urlIcon = 'assets/icons/icon_sam_question.png';
  }
  IconsSam.iconQuestion({this.height: 30, this.width: 30}) {
    urlIcon = 'assets/icons/icon_question.png';
  }
  IconsSam.textSam({this.height: 30, this.width: 30}) {
    urlIcon = 'assets/icons/icon_text_sam.png';
  }
  IconsSam.samBlue({this.height: 30, this.width: 30}) {
    urlIcon = 'assets/icons/icon_sam_blue.png';
  }
  IconsSam.iconSamLve({this.height: 30, this.width: 30}) {
    urlIcon = 'assets/icons/icon_sam_love.png';
  }


  String urlIcon;
  double height;
  double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.contain,
          image: AssetImage(urlIcon),
        ),
      ),
    );
  }
}
