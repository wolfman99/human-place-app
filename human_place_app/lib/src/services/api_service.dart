
import 'package:human_place_app/src/utils/keys.dart';
import 'package:youtube_api/youtube_api.dart';

class APIService {
  APIService._instantiate();
  static final APIService instance = APIService._instantiate();

  YoutubeAPI ytApi = new YoutubeAPI(
    API_KEY
  );

  Future<YT_API> fetchVideo({String urlVideo}) async {
    List<YT_API> ytResult = [];
    ytResult = await ytApi.search(urlVideo, type:  "video");
    return ytResult.first;
  }

  String getURL(String kind, String id) {
    String baseURL = "https://www.youtube.com/";
    switch (kind) {
      case 'channel':
        return "${baseURL}watch?v=$id";
        break;
      case 'video':
        return "${baseURL}watch?v=$id";
        break;
      case 'playlist':
        return "${baseURL}watch?v=$id";
        break;
    }
    return baseURL;
  }
}